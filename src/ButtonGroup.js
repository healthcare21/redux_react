import React, { Component } from 'react'
import {setTechnology} from "./actions"
import {store} from "./store"
import {connect} from 'react-redux'
import HelloWorld from "./HelloWorld";


class ButtonGroup extends Component {

  dispatchBtnAction(e) {
    const tech = e.target.dataset.tech;
    store.dispatch(setTechnology(tech));
  }
  render() {
    return (
    <div>
        <HelloWorld key={1} tech={store.getState().tech}/>
        {this.props.technologies.map((tech, i) => (
          <button
            data-tech={tech}
            key={`btn-${i}`}
            className="hello-btn"
            onClick={this.dispatchBtnAction}
          >
            {tech}
          </button>
        ))}
      </div>
    )
  }    
}

const mapStateToProps = state => ({
  tech:state.tech
})



export default connect(
  mapStateToProps
)(ButtonGroup)